<?php
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: *");  
header("Access-Control-Allow-Headers: Content-Type");
$servername = "localhost";
$username = "root";
$password = "";
$DBname = "StudentDB";

$conn = new mysqli($servername, $username, $password, $DBname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $requestData = $_POST;
  // $requestData = json_decode(file_get_contents('php://input'), true);
    
        $Student_id = $requestData['studentID'];

        if(DeleteStudent($Student_id, $conn)) {
            echo json_encode(['success' => true]);
        }
        else {
            echo json_encode(['success' => false, 'message' => 'No students found']);
        }
       
} else {
    echo 'Invalid request';
}

function DeleteStudent($Student_id, $conn) : bool {
    $sql = "DELETE FROM `Students` WHERE `id` = '$Student_id'";
    if($conn->query($sql) === TRUE) {
        return true;
    }
    else {
        return false;
    }
}

?>