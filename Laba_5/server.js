const express = require('express')


const app = express();
app.set('view engine', 'ejs')
app.use(express.urlencoded({extended: true}))
app.use(express.static('public'))
const PORT = 3002
const HOST = 'localhost';


const http = require('http');
const socketIO = require('socket.io');


const server = http.createServer(app);
const io = socketIO(server);

let uname;
let all_users = [];




app.get('/', (req, res) => {
    res.render('registration')
})

app.get('/page', (req, res) => {
    res.render('page', {name: uname})
})

let users = {} 
app.post('/Registration', (req, res)=>{
    console.log(req.body)
    uname = req.body.uname
   
    all_users.push(req.body.uname)
    console.log(all_users)
    res.redirect('/page')
})


app.get('/chats', (req, res)=>{
    res.render('chat', {name: uname})
})

app.get('/tasks', (req, res)=>{
    res.render('tasks', {name:uname})
})

app.get('/student', (req, res)=>{
    res.render('page', {name: uname})
})


app.get('/Logout', (req, res)=>{
    res.render('registration')
})


let rooms = {};
let i = 0;
io.on('connection', socket => {
   // users[socket.id] = all_users[i];
   users[socket.id] = uname;
    ++i;
    let name = users[socket.id]
    console.log(users)
   
    console.log('Клієнт підключився успішно');
    socket.on('send-chat-message', message =>{
       socket.to(message.roomName).emit('chat-message', {message: message.mess, name: users[socket.id]});
    })
    
    // socket.on('disconnect', () =>{
    //     socket.broadcast.emit('user-disconnect', users[socket.id]);
    //     console.log(`ds  ${users[socket.id]}`)
    //     delete rooms[]users[socket.id];
    //  })
     socket.on('disconnect', () => {
        getUserRooms(socket).forEach(room => {
          socket.to(room).emit('user-disconnect', rooms[room].users[socket.id])
          delete rooms[room].users[socket.id]
        })
      })


     socket.on('new-room', room =>{
        rooms[room.roomName] = {users:{}};
        console.log(rooms)
        socket.broadcast.emit('add-new-room',room);
     } )

     
     socket.on('join-to-room', data =>{
      socket.join(data.roomName);
      rooms[data.roomName].users[socket.id] = data.userName
        console.log(data);
        console.log(rooms);
        socket.broadcast.emit('add-icon',data);
     } )



     //////////////////////////////////////////////////////////
     // таски
    socket.on('add-task', task =>{
        socket.broadcast.emit('add-new-task', {board: task.board, name: task.name, date: task.date, text: task.text});
    })

    socket.on('edit-task', editedTask => {
        console.log(editedTask)
        socket.broadcast.emit('update-task', editedTask);
    });
});

server.listen(PORT, HOST, () => {
    console.log(`Сервер запущений на: http://${HOST}:${PORT}`);
});

function getUserRooms(socket) {
    return Object.entries(rooms).reduce((names, [name, room]) => {
      if (room.users[socket.id] != null) names.push(name)
      return names
    }, [])
  }