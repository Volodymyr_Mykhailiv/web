<?php

$servername = "localhost";
$username = "root";
$password = "";
$DBname = "StudentDB";

$conn = new mysqli($servername, $username, $password, $DBname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $requestData = $_POST;
  // $requestData = json_decode(file_get_contents('php://input'), true);
    
        $studentsData = getAllStudentsData($conn);
        if ($studentsData !== null) {
            echo json_encode(['success' => true, 'students' => $studentsData]);
        } else {
            echo json_encode(['success' => false, 'message' => 'No students found']);
        }
    

} else {
    echo 'Invalid request';
}







function getAllStudentsData($conn) {
    $sql = "SELECT * FROM `Students`";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $studentsData = array(); // Масив для зберігання даних про студентів

        while ($row = $result->fetch_assoc()) {
            $studentsData[] = $row; // Додавання даних про кожного студента до масиву
        }

        return $studentsData;
    } else {
        return null; 
    }
}


?>