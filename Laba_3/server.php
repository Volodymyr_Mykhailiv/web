<?php

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $requestData = $_POST;
  // $requestData = json_decode(file_get_contents('php://input'), true);
    
    $errors = array();
    // if(ValidateGroup($requestData['group']) && ValidateName($requestData['name']) 
    // && ValidateSurname($requestData['surname']) && ValidateGender($requestData['gender'])
    //  && ValidateBirth($requestData['birthday'])) {
        ValidateGroup($requestData['group']);
        ValidateName($requestData['name']);
        ValidateSurname($requestData['surname']);
        ValidateGender($requestData['gender']);
        ValidateBirth($requestData['birthday']);

        if($errors) {
            $response['errors'] = $errors;
            echo json_encode(['success' => false, 'errors' => $response['errors']]);
        }
     else {
        echo json_encode(['success' => true]);
        }
} else {
    echo 'Invalid request';
}


function ValidateGroup($group) {
    global $errors;
    if($group == "") {
        $errors[] = 'Group is required';
        return false;
    }
    return true;
}

function ValidateName($name) {
    global $errors;
    if($name == "") {
        $errors[] = 'Name is required';
        return false;
    }
    else if(strlen($name) < 2) {
        $errors[] = 'Name must have more then 2 symbols';
        return false;
    }
    else if (!preg_match('/^[A-Z][a-z]+$/', $name)) {
        $errors[] = 'Name must start with a capital letter and contain only letters';
        return false;
    }
    return true;
}

function ValidateSurname($surname) {
    global $errors;
    if($surname == "") {
        $errors[] = 'Surname is required';
        return false;
    }
    else if(strlen($surname) < 2) {
        $errors[] = 'Surname must have more then 2 symbols';
        return false;
    }
    else if (!preg_match('/^[A-Z][a-z]+$/', $surname)) {
        $errors[] = 'Surname must start with a capital letter and contain only letters';
        return false;
    }
    return true;
}

function ValidateGender($gender) {
    global $errors;
    if($gender == "") {
        $errors[] = 'Gender is required';
        return false;
    }
    return true;
}

function ValidateBirth($birthday) {
    global $errors;
    if($birthday == "") {
        $errors[] = 'Birthday is required';
        return false;
    }
    else if(strtotime($birthday) > time()) {
        $errors[] = 'Birthdate cannot be in the future';
        return false;
    } 
    else if(strtotime($birthday) > strtotime('-16 years')) {
        $errors[] = 'You must be at least 16 years old';
        return false;
    } 
    else if(strtotime($birthday) < strtotime('-70 years')) {
        $errors[] = 'You must be no more than 70 years old';
        return false;
    }
    return true;
}
