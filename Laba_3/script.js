
// видалення рядка з таблиці

function DeleteElem() {
// Отримати всі кнопки btn-delete
var deleteButtons = document.querySelectorAll('.btn-delete');

// Пройтися по кожній кнопці і додати обробник подій для кліків
deleteButtons.forEach(function(button) {
    button.addEventListener('click', function() {
    var row = button.parentNode.parentNode;

    // Отримати дані з комірок рядка
    var studentName = row.cells[2].textContent;

    // Вставити дані в текст модального вікна
    var modalBody = document.getElementById('confirm-text');
    modalBody.innerHTML = `<p>Are you sure you want to delete user <b>${studentName}</b>?</p>`;

    // Відобразити модальное вікно
    var confirmDeletion = document.getElementById("warning-modal");
      if(confirmDeletion.style.display !== "block") {
            confirmDeletion.style.display = "block";
          }
    });
});

var closeWindowDeletion =  document.querySelectorAll('.close')
closeWindowDeletion.forEach(function(button) {
    button.addEventListener('click', function() {
        document.getElementById("warning-modal").style.display = "none";
    });
});


var deleteButtons = document.querySelectorAll('.btn-delete');
deleteButtons.forEach(function(button) {
    button.addEventListener('click', function() {
        var rowToDelete = this.closest('tr');

        // Додати обробник подій для кнопки "Видалити" у модальному вікні
        var confirmButton = document.getElementById('ok-button');
        confirmButton.addEventListener('click', function() {
            // Видалити знайдений рядок
            if (rowToDelete) {
                rowToDelete.remove();
            }
            // Закрити модальне вікно підтвердження видалення
            document.getElementById("warning-modal").style.display = "none"
        });
    });
});

// видалення
}

///////////////////////////////////////////////////////////////////////////////////////////
//додавання
function CloseWindowAdd() {
    var close = document.getElementById("add-modal");
    if(close.style.display !== "none") {
        close.style.display = "none"
    }
}

function AddStudent() {
var showModalWindow = document.getElementById("add-student-btn");
var formTitle = document.getElementById("form-title");
showModalWindow.addEventListener('click',function(){
    formTitle.innerHTML = "Add student";
    btn_save = document.getElementById("ok-button-add");
    btn_save.innerHTML = "Ok";
    if(btn_save.textContent === "Ok"){
        document.getElementById("ok-button-edit").style.display = "none";
   }
   document.getElementById("ok-button-add").style.display = "inline";
   
    document.getElementById("group").value = "";
    document.getElementById("name").value = "";
    document.getElementById("surname").value = "";
    document.getElementById("gender").value = "M";
    document.getElementById("birthday").value = "";

    var Show = document.getElementById("add-modal");
    if(Show.style.display !== "block") {
        Show.style.display = "block";
    }
});



const studentsTable = document.getElementById("students-table");
const confirmAdding = document.getElementById("ok-button-add");

confirmAdding.addEventListener('click', function() {
    const id = document.getElementById("student-id").value;
    const group =  document.getElementById("group").value;
    const name = document.getElementById("name").value;
    const surname = document.getElementById("surname").value;
    const gender = document.getElementById("gender").value;
    const birthday = document.getElementById("birthday").value;

    const data = {
        id: id,
        group: group,
        name: name,
        surname: surname,
        gender: gender,
        birthday: birthday
    };
    
    handleFormSubmission(data); // Виклик функції для обробки ajax запиту та форми
});
}

// $.ajax({
//     url: 'server.php',
//     type: 'POST',
//     //contentType: 'application/json', 
//     data: data,
//    // data: JSON.stringify(data), // Перетворення об'єкта у JSON строку
//     success: function(response) {
//         var parsedResponse = JSON.parse(response); // Розпарсити JSON відповідь
//         if (parsedResponse.success) {
//             console.log('Success:', parsedResponse);

//         } else {
//             console.log("проблема при валідації");
//             console.error('Failure:', parsedResponse.errors);
//             for(var i = 0; i < parsedResponse.errors.length; i++) {
//                 alert(parsedResponse.errors[i]);
//             }
//         }
//     },
//     error: function(xhr, status, error) {
//         console.error('Error sending request:', error);
//     }
// });


//     const newRow = document.createElement('tr');
//     newRow.innerHTML = ` <td>  
//     <form>
//         <label for="checkbox2" aria-label="Checkbox Label"></label>
//         <input type="checkbox" id="checkbox2">
//     </form>
// </td>
// <td>${group}</td>   
// <td>${name} ${surname}</td>
// <td>${gender}</td>
// <td>${birthday}</td>
// <td><div class="status-circle status-color"></div></td>
// <td>
//     <button class="btn-edit"> <img class="img-pencil" src="/Images/Pencil.png" alt="pencil"></button>
//     <button class="btn-delete"> <img class="img-delete" src="/Images/delete.png" alt="delete"> </button>
// </td>`;
// if(btn_save.textContent === "Ok"){
// studentsTable.appendChild(newRow);
// }

// var close = document.getElementById("add-modal");
// close.style.display = "none";


// // Додати обробники подій для нових кнопок видалення та редагування
// EditStudent();
// DeleteElem();

// });
// }




function handleFormSubmission(data) {
    $.ajax({
    url: 'server.php',
    type: 'POST',
    //contentType: 'application/json', 
    data: data,
   // data: JSON.stringify(data), // Перетворення об'єкта у JSON строку
    success: function(response) {
        var parsedResponse = JSON.parse(response); // Розпарсити JSON відповідь
        if (parsedResponse.success) {
            console.log('Success:', parsedResponse);
            if ($("#ok-button-add").is(":visible")) {
            addStudentToTable(data);
            }
        } else {
            console.log("проблема при валідації");
            console.error('Failure:', parsedResponse.errors);
            for(var i = 0; i < parsedResponse.errors.length; i++) {
                alert(parsedResponse.errors[i]);
            }
        }
    },
    error: function(xhr, status, error) {
        console.error('Error sending request:', error);
    }
});
}


function addStudentToTable(data) {
    const studentsTable = document.getElementById("students-table");
    
    const newRow = document.createElement('tr');
    newRow.innerHTML = ` <td>  
        <form>
            <label for="checkbox2" aria-label="Checkbox Label"></label>
            <input type="checkbox" id="checkbox2">
        </form>
    </td>
    <td>${data.group}</td>   
    <td>${data.name} ${data.surname}</td>
    <td>${data.gender}</td>
    <td>${data.birthday}</td>
    <td><div class="status-circle status-color"></div></td>
    <td>
        <button class="btn-edit"> <img class="img-pencil" src="/Images/Pencil.png" alt="pencil"></button>
        <button class="btn-delete"> <img class="img-delete" src="/Images/delete.png" alt="delete"> </button>
    </td>`;
    if(btn_save.textContent === "Ok"){
    studentsTable.appendChild(newRow);
    }

    var close = document.getElementById("add-modal");
    close.style.display = "none";


    // Додати обробники подій для нових кнопок видалення та редагування
    EditStudent();
    DeleteElem();
}


//  function EditStudent() {
//     // Отримуємо всі кнопки "Edit"
//     var editButtons = document.querySelectorAll(".btn-edit");

//     // Додаємо обробник подій до кожної кнопки "Edit"
//     editButtons.forEach(function(button) {
//         button.addEventListener('click', function() {
//        // $(".btn-edit").click(function() {
//             // Отримуємо доступ до заголовку форми
//             var formTitle = document.getElementById("form-title");
//             formTitle.innerHTML = "Edit student";

//             // Отримуємо доступ до кнопки "Save"
//             var btn_save = document.getElementById("ok-button-edit");
//             btn_save.innerHTML = "Save";

//              // Перевіряємо, чи є попередні обробники подій і видаляємо їх
//             //  if (btn_save.clickHandler) {
//             //     btn_save.removeEventListener('click', btn_save.clickHandler);
//             // }

//             document.getElementById("ok-button-add").style.display = "none";
//             document.getElementById("ok-button-edit").style.display = "inline";

//             // Знаходимо ближній рядок до кнопки "Edit"
//             var currentRow = this.closest('tr');
//             var cells = currentRow.cells;
//             // Отримуємо дані студента з поточного рядка
//             var group = cells[1].textContent;
//             var name = cells[2].textContent.split(' ')[0];
//             var surname = cells[2].textContent.split(' ')[1];
//             var gender = cells[3].textContent;
//             var birthday = cells[4].textContent;

//             // Заповнюємо поля форми даними студента з поточного рядка
//             document.getElementById("group").value = group;
//             document.getElementById("name").value = name;
//             document.getElementById("surname").value = surname;
//             document.getElementById("gender").value = gender;
//             document.getElementById("birthday").value = birthday;

//             var Show = document.getElementById("add-modal");
//             Show.style.display = "block";

//             btn_save.onclick = function() {
//             //var saveHandler = function() {
//                 // Перевіряємо валідацію перед збереженням
//                 if (!validateFields()) {
//                     return;
//                 }
//                 // Оновлюємо дані студента в поточному рядку
//                 cells[1].textContent = document.getElementById("group").value;
//                 cells[2].textContent = document.getElementById("name").value + " " + document.getElementById("surname").value;
//                 cells[3].textContent = document.getElementById("gender").value;
//                 cells[4].textContent = document.getElementById("birthday").value;

//                 document.getElementById("add-modal").style.display = "none";
//                // btn_save.removeEventListener('click', saveHandler);
//            // };
//            // btn_save.clickHandler = saveHandler;
//             // Додаємо обробник подій для кнопки "Save"
//             //btn_save.addEventListener('click', saveHandler);
//         }
//         });
//     });
// }






 ///////////
 // Редагування
//  function EditStudent() {
//     $(".btn-edit").click(function() {
//         // Отримуємо доступ до заголовку форми
//         $("#form-title").text("Edit student");

//         // Отримуємо доступ до кнопки "Save"
//         $("#ok-button-edit").text("Save");

//         // Перевіряємо, чи є попередні обробники подій і видаляємо їх
//         var btn_save = $("#ok-button-edit");
//         if (btn_save.clickHandler) {
//             $("#ok-button-edit").off('click', btn_save.clickHandler);
//         }

//         $("#ok-button-add").hide();
//         $("#ok-button-edit").show();

//         // Знаходимо ближній рядок до кнопки "Edit"
//         var currentRow = $(this).closest('tr');
//         var cells = currentRow.find('td');
//         // Отримуємо дані студента з поточного рядка
//         var group = cells.eq(1).text();
//         var name = cells.eq(2).text().split(' ')[0];
//         var surname = cells.eq(2).text().split(' ')[1];
//         var gender = cells.eq(3).text();
//         var birthday = cells.eq(4).text();

//         // Заповнюємо поля форми даними студента з поточного рядка
//         $("#group").val(group);
//         $("#name").val(name);
//         $("#surname").val(surname);
//         $("#gender").val(gender);
//         $("#birthday").val(birthday);

//         $("#add-modal").show();

//         var saveHandler = function() {
//             //Перевіряємо валідацію перед збереженням
//             if (!validateFields()) {
//                 return;
//             }
//             // Оновлюємо дані студента в поточному рядку
//             cells.eq(1).text($("#group").val());
//             cells.eq(2).text($("#name").val() + " " + $("#surname").val());
//             cells.eq(3).text($("#gender").val());
//             cells.eq(4).text($("#birthday").val());

//             $("#add-modal").hide();
//             $(this).off('click', saveHandler);
//         };
//         btn_save.clickHandler = saveHandler;
//         // Додаємо обробник подій для кнопки "Save"
//         $("#ok-button-edit").on('click', saveHandler);
//     });
// }


document.addEventListener("DOMContentLoaded", function() {
    AddStudent();
    DeleteElem();
    EditStudent();
});


// Функція перевірки валідації
function validateFields() {
    var group = document.getElementById("group").value;
    var name = document.getElementById("name").value;
    var surname = document.getElementById("surname").value;
    var birthday = document.getElementById("birthday").value;

    if (!group || !name || !surname || !birthday) {
        alert("Please fill out all fields.");
        return false;
    }

    const invalidCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
    if (invalidCharacters.test(name) || invalidCharacters.test(surname)) {
        alert("Invalid characters detected in name or surname.");
        return false;
    }
    var parts = birthday.split('-');
    var year = parseInt(parts[0]);
    // Перевіряємо, чи рік народження знаходиться в припустимому діапазоні
    if (year < 1950 || year > 2006) {
        alert("Please enter a valid birth year between 1950 and 2006.");
        return false;
    }

    if (!startsWithCapitalAndAllLower(name) || !startsWithCapitalAndAllLower(surname)) {
        alert("Name and surname should start with a capital letter, and other letters should be lower.");
        return false;
        
    }
    return true;
}

function startsWithCapitalAndAllLower(str) {
    // Перевіряємо, чи перший символ - велика літера
    if (str.charAt(0) !== str.charAt(0).toUpperCase()) {
        return false;
    }
    
    // Перевіряємо, чи всі наступні символи - малі літери
    var restOfStr = str.slice(1);
    return restOfStr === restOfStr.toLowerCase();
}




 function EditStudent() {
    var editButtons = document.querySelectorAll(".btn-edit");
    // Додаємо обробник подій до кожної кнопки "Edit"
    editButtons.forEach(function(button) {
        button.addEventListener('click', function() {
           
            var formTitle = document.getElementById("form-title");
            formTitle.innerHTML = "Edit student";

            var btn_save = document.getElementById("ok-button-edit");
            btn_save.innerHTML = "Save";

            document.getElementById("ok-button-add").style.display = "none";
            document.getElementById("ok-button-edit").style.display = "inline";

            // Знаходимо ближній рядок до кнопки "Edit"
            var currentRow = this.closest('tr');
            var cells = currentRow.cells;
            // Отримуємо дані студента з поточного рядка
            var _group = cells[1].textContent;
            var _name = cells[2].textContent.split(' ')[0];
            var _surname = cells[2].textContent.split(' ')[1];
            var _gender = cells[3].textContent;
            var _birthday = cells[4].textContent;

            // Заповнюємо поля форми даними студента з поточного рядка
            document.getElementById("group").value = _group;
            document.getElementById("name").value = _name;
            document.getElementById("surname").value = _surname;
            document.getElementById("gender").value = _gender;
            document.getElementById("birthday").value = _birthday;

            var Show = document.getElementById("add-modal");
            Show.style.display = "block";

      
        btn_save.onclick = function() {
            const id = document.getElementById("student-id").value;
            const group =  document.getElementById("group").value;
            const name = document.getElementById("name").value;
            const surname = document.getElementById("surname").value;
            const gender = document.getElementById("gender").value;
            const birthday = document.getElementById("birthday").value;
        
            const data = {
                id: id,
                group: group,
                name: name,
                surname: surname,
                gender: gender,
                birthday: birthday
            };
            $.ajax({
                url: 'server.php',
                type: 'POST',
                //contentType: 'application/json', 
                data: data,
               // data: JSON.stringify(data), // Перетворення об'єкта у JSON строку
                success: function(response) {
                    var parsedResponse = JSON.parse(response); // Розпарсити JSON відповідь
                    if (parsedResponse.success) {
                        console.log('Success:', parsedResponse);
                        
                            // Оновлюємо дані студента в поточному рядку
                            cells[1].textContent = document.getElementById("group").value;
                            cells[2].textContent = document.getElementById("name").value + " " + document.getElementById("surname").value;
                            cells[3].textContent = document.getElementById("gender").value;
                            cells[4].textContent = document.getElementById("birthday").value;
            
                             document.getElementById("add-modal").style.display = "none";
                            
                    } else {
                        console.log("проблема при валідації");
                        console.error('Failure:', parsedResponse.errors);
                        for(var i = 0; i < parsedResponse.errors.length; i++) {
                            alert(parsedResponse.errors[i]);
                        }
                    }
                },
                error: function(xhr, status, error) {
                    console.error('Error sending request:', error);
                }
            });
        };
    });   
  });
 }
 