
// видалення рядка з таблиці

// Отримати всі кнопки btn-delete
var deleteButtons = document.querySelectorAll('.btn-delete');

// Пройтися по кожній кнопці і додати обробник подій для кліків
deleteButtons.forEach(function(button) {
    button.addEventListener('click', function() {
    var row = button.parentNode.parentNode;

    // Отримати дані з комірок рядка
    var studentName = row.cells[2].textContent;

    // Вставити дані в текст модального вікна
    var modalBody = document.getElementById('confirm-text');
    modalBody.innerHTML = `<p>Are you sure you want to delete user <b>${studentName}</b>?</p>`;

    // Відобразити модальное вікно
    var confirmDeletion = document.getElementById("warning-modal");
      if(confirmDeletion.style.display !== "block") {
            confirmDeletion.style.display = "block";
          }
    });
});

var closeWindowDeletion =  document.querySelectorAll('.close')
closeWindowDeletion.forEach(function(button) {
    button.addEventListener('click', function() {
        document.getElementById("warning-modal").style.display = "none";
    })
})


var deleteButtons = document.querySelectorAll('.btn-delete');
deleteButtons.forEach(function(button) {
    button.addEventListener('click', function() {
        var rowToDelete = this.closest('tr');

        // Додати обробник подій для кнопки "Видалити" у модальному вікні
        var confirmButton = document.getElementById('ok-button');
        confirmButton.addEventListener('click', function() {
            // Видалити знайдений рядок
            if (rowToDelete) {
                rowToDelete.remove();
            }
            // Закрити модальне вікно підтвердження видалення
            document.getElementById("warning-modal").style.display = "none"
        });
    });
});

// видалення

///////////////////////////////////////////////////////////////////////////////////////////
//додавання

var showModalWindow = document.getElementById("add-student-btn");
showModalWindow.addEventListener('click',function(){
    var Show = document.getElementById("add-modal");
    if(Show.style.display !== "block") {
        Show.style.display = "block";
    }
});

function CloseWindowAdd() {
    var close = document.getElementById("add-modal");
    if(close.style.display !== "none") {
        close.style.display = "none"
    }
}

const studentsTable = document.getElementById("students-table");
const confirmAdding = document.getElementById("ok-button-add");
confirmAdding.addEventListener('click', function() {
    const group =  document.getElementById("group").value;
    const name = document.getElementById("name").value;
    const surname = document.getElementById("surname").value;
    const gender = document.getElementById("gender").value;
    const birthday = document.getElementById("birthday").value;


    const newRow = document.createElement('tr');
    newRow.innerHTML = ` <td>  
    <form>
        <label for="checkbox2" aria-label="Checkbox Label"></label>
        <input type="checkbox" id="checkbox2">
    </form>
</td>
<td>${group}</td>
<td>${name} ${surname}</td>
<td>${gender}</td>
<td>${birthday}</td>
<td><div class="status-circle status-color"></div></td>
<td>
    <button class="btn-edit"> <img class="img-pencil" src="/Images/Pencil.png" alt="pencil"></button>
    <button class="btn-delete"> <img class="img-delete" src="/Images/delete.png" alt="delete"> </button>
</td>`;
studentsTable.appendChild(newRow);


// Додати обробники подій для нових кнопок видалення та редагування
newRow.querySelectorAll('.btn-delete').forEach(function(button) {
    button.addEventListener('click', function() {
        var row = button.parentNode.parentNode;

        // Отримати дані з комірок рядка
        var studentName = row.cells[2].textContent;

        var modalBody = document.getElementById('confirm-text');
        modalBody.innerHTML = `<p>Are you sure you want to delete user <b>${studentName}</b>?</p>`;

        var confirmDeletion = document.getElementById("warning-modal");
        if (confirmDeletion.style.display !== "block") {
            confirmDeletion.style.display = "block";
        }

        // Додати обробник події для кнопки підтвердження видалення
        var confirmButton = document.getElementById('ok-button');
        confirmButton.addEventListener('click', function() {
            // Видалити знайдений рядок
            if (row) {
                row.remove();
            }
            document.getElementById("warning-modal").style.display = "none"
        });
    });
});
    
var close = document.getElementById("add-modal");
close.style.display = "none";
});

